FROM ruby:2.7

# Update installed packages and install nodejs
# default DEPLOYABLE
# is null for local build and
# development
ARG DEPLOYABLE

# https://github.com/rails/rails/issues/32947
# we can pass actual value from CI ENV as build-arg if needed
ARG SECRET_KEY_BASE=1

# Update installed packages and install nodejs
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
      && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
      && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
      && apt-get update -qq \
      && apt-get upgrade -y \
      && apt-get install -y --no-install-recommends \
        build-essential \
        git \
        postgresql-client \
        libpq5 \
        libpq-dev \
        yarn \
        nodejs \
      && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives

ENV APP_PATH /opt/app-root/src

WORKDIR ${APP_PATH}

# Allow sassc gem to work cross-platform:
# https://github.com/sass/sassc-ruby/issues/146
ENV BUNDLE_BUILD__SASSC=--disable-march-tune-native

COPY Gemfile* ${APP_PATH}/
RUN bundle install --retry 3 \
      && rm -rf `gem env gemdir`/cache/*.gem \
      && find `gem env gemdir`/gems/ -name "*.c" -delete \
      && find `gem env gemdir`/gems/ -name "*.o" -delete

# Copy the application into the container
COPY . $APP_PATH

RUN if [ -n "${DEPLOYABLE}" ]; then \
      RAILS_ENV=production rails assets:precompile && \
      yarn install --check-files --frozen-lockfile --non-interactive && \
      rm -rf tmp/cache vendor/assets; fi

RUN chmod -R g=u ${APP_PATH}

EXPOSE 3000
