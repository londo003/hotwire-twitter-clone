#!/bin/bash
release_name=$1
if [ -z "${release_name}" ]
then
    echo "usage: deploy-production-middleware.sh <release_name>" >&2
    exit
fi
redis__password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
fullnameOverride='production-hotwire-twitter-clone'
DB__USER=postgres
DB__PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
DB__NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
helm template ${release_name} helm-chart/hotwire-twitter-clone -s templates/redis_deployment.yaml -s templates/redis_service.yaml -s templates/db_deployment.yaml -s templates/db_service.yaml --set-string redis.password=${redis__password} --set-string fullnameOverride=${fullnameOverride} --set-string db.user="${DB__USER}" --set-string db.password="${DB__PASSWORD}" --set-string db.name="${DB__NAME}" --set-string db.server="notused" | kubectl apply -f -
echo DATABASE_URL=$(kubectl get service ${fullnameOverride}-db -o json | jq -r "\"postgres://${DB__USER}:${DB__PASSWORD}@\"+.metadata.name+\":\"+(.spec.ports[0].port|tostring)")
echo REDIS_URL=$(kubectl get service ${fullnameOverride}-redis -o json | jq -r "\"redis://:${redis__password}@\"+.metadata.name+\":\"+(.spec.ports[0].port|tostring)+\"/1\"")