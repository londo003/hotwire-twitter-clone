# This file is used by Rack-based servers to start the application.

require_relative "config/environment"

map '/liveness' do
    run Proc.new { |env| ['200', {'Content-Type' => 'text/html'}, ['its alive!']] }
end

run Rails.application
Rails.application.load_server
