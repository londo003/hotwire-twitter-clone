#!/bin/bash
release_name=$1
if [ -z "${release_name}" ]
then
    echo "usage: deploy-production-middleware.sh <release_name>" >&2
    exit
fi
kubectl delete all -l app.kubernetes.io/instance=${release_name}