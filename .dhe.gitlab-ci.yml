stages:
  - build
  - test
  - deploy

variables:
  LOG_LEVEL: "info"
  CANDIDATE_IMAGE: "${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"
  WANTS_DATABASE: 1
  HELM_CHART_NAME: hotwire-twitter-clone

include:
  - project: 'utility/project-templates/ci-templates'
    file: '/docker.yml'
  - project: 'ori-rad/ci-pipeline-utilities/deployment'
    file: '/deployment.yml'

build:
  stage: build
  before_script:
    - export THE_DOCKERFILE="Dockerfile"
    - export THE_IMAGE="${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}"
    - export BUILD_ARGS="--build-arg DEPLOYABLE=true --build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg CI_PROJECT_URL=${CI_PROJECT_URL}"
  extends: .kaniko_build
  only:
    - branches
  retry:
    max: 2
    when: runner_system_failure

scan:
  stage: test
  extends: .docker_scan
  before_script:
    - export THE_DOCKERFILE="Dockerfile"
    - export THE_IMAGE="${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}"
  only:
    - branches
  retry:
    max: 2
    when: runner_system_failure

test:
  stage: test
  services:
    - name: bitnami/postgresql:11-debian-10
      alias: postgres
  image: ${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}
  variables:
    GIT_STRATEGY: none
  variables:
    RAILS_ENV: test
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: abc123
    DATABASE_USER: ${POSTGRES_USER}
    DATABASE_PASSWORD: ${POSTGRES_PASSWORD}
    DATABASE_HOST: postgres
  script:
    - cd ${APP_PATH}
    - unset DATABASE_URL
    - rails db:create
    - rails db:migrate
    - rails test
    - mv coverage ${CI_PROJECT_DIR}/coverage
  artifacts:
    paths:
      - coverage
  tags:
    - kube-executor
  only:
    refs:
      - branches
  retry:
    max: 2
    when: runner_system_failure

.deploy_to_cluster:
  stage: deploy
  extends: .deploy
  retry:
    max: 2
    when: runner_system_failure

.decommission_from_cluster:
  stage: deploy
  extends: .decommission
  environment:
    action: stop
  when: manual
  retry:
    max: 2
    when: runner_system_failure

deploy_review:
  extends: .deploy_to_cluster
  variables:
    APPLICATION_SPECIFIC_ENVIRONMENT: redis__password
  before_script:
    - export DB__USER=postgres
    - export DB__PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    - export DB__NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
    - source /usr/local/bin/deploy
    - export DB__SERVER="$(deployment_name)-db"
    - export redis__password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
  environment:
    name: ${CI_COMMIT_REF_SLUG}
    url: https://${CI_ENVIRONMENT_SLUG}-${HELM_CHART_NAME}.ocp.dhe.duke.edu
    on_stop: decommission_review
  only:
    refs:
      - branches
    variables:
      - $HELM_TOKEN
  except:
    - master

decommission_review:
  extends: .decommission_from_cluster
  environment:
    name: ${CI_COMMIT_REF_SLUG}
  only:
    refs:
      - branches
    variables:
      - $HELM_TOKEN
  except:
    - master

deploy_production:
  extends: .deploy_to_cluster
  variables:
    APPLICATION_SPECIFIC_ENVIRONMENT: "redis__url"
  environment:
    name: production
    url: https://${HELM_CHART_NAME}.ocp.dhe.duke.edu
  only:
    refs:
      - master
    variables:
      - $HELM_TOKEN
